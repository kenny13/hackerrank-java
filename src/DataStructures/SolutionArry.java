package DataStructures;

public class SolutionArry {

    public static void main(String []argh) {

        System.out.println("Hellow World");
        int [] a = {1,2,3,4,1};
        reverseArray(a);

    }

    static int[] reverseArray(int[] a) {
        int [] b = new int[a.length];
        int counter = 0;
        for (int i = a.length ; i > 0 ; i--) {
            b[counter] = a[i-1];
            counter = counter+1;
        }
        return b;
    }

}
