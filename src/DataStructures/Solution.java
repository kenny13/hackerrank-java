package DataStructures;

import java.io.IOException;

public class Solution {

    static int hourglassSum(int[][] arr) {
        int startX = 0;
        int startY = 0;

        int maxX = arr[0].length;
        int maxY = arr.length;

        int hourglassArr [] = new int [((maxX-2) * (maxY-2))];

        for (int z = 0; z < ((maxX-2) * (maxY-2) ) ; z++) {

            int hourglass = 0;
            for (int y = startY; y < (startY+3) ; y++) {
                for (int x = startX; x < (startX+3) ; x++) {
                    System.out.print(arr[y][x]+ " ");

                    if (((y == startY) || (y == startY+2))
                        || ((y == startY+1) && (x == startX+1)))
                        hourglass = hourglass + arr[y][x];

                }
                System.out.println();
            }

            System.out.println("-------------------:"+ hourglass);
            startX = startX+1;
            if (startX == (maxX-2)){
                startX = 0;
                startY = startY +1;
                System.out.println("=========================");
            }

            hourglassArr[z] = hourglass;

        }

        int max = -99 ;
        for (int sk : hourglassArr)
            if (sk > max)
                max=sk;

        System.out.println(max);
        return max;

    }

    public static void main(String[] args) throws IOException {

        int [][] arr ={
                {1, 1, 1, 0, 0, 0},
                {0, 1, 0, 0, 0, 0},
                {1, 1, 1, 0, 0, 0},
                {0, 0, 2, 4, 4, 0},
                {0, 0, 0, 2, 0, 0},
                {0, 0, 1, 2, 4, 0}
        };

        int [][] arr2 ={
                {-9, -9, -9,  1, 1, 1 },
                { 0, -9,  0,  4, 3, 2 },
                {-9, -9, -9,  1, 2, 3 },
                {0,   0,  8,  6, 6, 0 },
                {0,   0,  0, -2, 0, 0 },
                {0,   0,  1,  2, 4, 0 }
        };

        int [][] arr3 ={
                {-1, -1,  0, -9, -2, -2},
                {-2, -1, -6, -8, -2, -5},
                {-1, -1, -1, -2, -3, -4},
                {-1, -9, -2, -4, -4, -5},
                {-7, -3, -3, -2, -9, -9},
                {-1, -3, -1, -2, -4, -5}
        };

        hourglassSum(arr3);


    }
}
