package Java;

import java.util.ArrayList;
import java.util.List;

public class SolutionOddNum {

    public static void main(String []argh) {

        List <Integer> list = oddNumbers(3,9);

        for (Integer i: list)
            System.out.println(i);
    }

    public static List<Integer> oddNumbers(int l, int r) {
        List<Integer> list = new ArrayList<Integer>();

        for (int i = l; i <= r; i++) {
            if ( (i % 2) != 0 )
                list.add(i);
        }

        return list;
    }

}
