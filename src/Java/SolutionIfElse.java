package Java;

import java.util.Scanner;

public class SolutionIfElse {

    private static final Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        int x = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        /*
        If  is odd, print Weird
        If  is even and in the inclusive range of 2 to , 5 print Not Weird
        If  is even and in the inclusive range of 6 to , 20 print Weird
        If  is even and greater than 20 , print Not Weird
         */
        if ( (x & 1) == 0 ) {
            //even
            if ((x >= 2 ) && (x<=5))
                System.out.println("Not Weird");

            if ((x >= 6 ) && (x <= 20))
                System.out.println("Weird");

            if ((x > 20))
                System.out.println("Not Weird");

        } else {
            //odd
            System.out.println("Weird");
        }




        scanner.close();
    }
}
