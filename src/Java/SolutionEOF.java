package Java;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SolutionEOF {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        List<String> list = new ArrayList<String>();

        while (scan.hasNext()) {
            String str = scan.next();
            str += scan.nextLine();
            list.add(str);
        }

        for (int j = 0; j < list.size(); j++) {
            String line = list.get(j);
            System.out.println((j+1) +" "+ line);
        }

    }

}
